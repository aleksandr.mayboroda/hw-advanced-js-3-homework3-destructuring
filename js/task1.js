const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
const parent = document.querySelector('.result')
// parent.insertA


parent.insertAdjacentHTML('beforeend','<h2 class="task-title">Задача 1</h2>')
let ol = document.createElement('ol'),
    // newArr = [...clients1,...clients2]
    newArr = Array.from(new Set([...clients1,...clients2]))
ol.className = 'result-list'
newArr.forEach(element => {
    ol.insertAdjacentHTML('beforeend',`<li>${element}</li>`)
});
parent.insertAdjacentElement('beforeend',ol)

console.log('task 1',newArr)