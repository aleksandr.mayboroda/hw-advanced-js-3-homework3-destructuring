const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
  }

const newEmployee = {...employee, age: 100500, salary: 0}

let parent6 = document.querySelector('.result'),
    str6 = ''

for(let key in newEmployee)
{
    str6 += `${key}: ${newEmployee[key]}<br>`
}

parent6.insertAdjacentHTML('beforeend','<h2 class="task-title">Задача 6</h2>')
parent6.insertAdjacentHTML('beforeend',`<p class="result-list">${str6}</p>`)

console.log('task6',newEmployee)