const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
];

let parent2 = document.querySelector('.result'),
    newArr2 = [],
    ol2 = document.createElement('ol')

    ol2.className = 'result-list'

parent2.insertAdjacentHTML('beforeend','<h2 class="task-title">Задача 2</h2>')


newArr2 = characters.map(char => {
    let {name,lastName,age} = char
    return {
        name,
        lastName,
        age
    }
})
newArr2.forEach(el => {
    let {name,lastName,age} = el
    ol2.insertAdjacentHTML('beforeend',`<li>${lastName} ${name} ${age} годиков</li>`)
})
parent2.insertAdjacentElement('beforeend',ol2)

console.log('task2',newArr2)
  