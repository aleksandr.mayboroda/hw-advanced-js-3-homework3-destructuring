const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
}

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}

let fullProfile = {
    ...satoshi2018,
    ...satoshi2019,
    ...satoshi2020,
},
parent4 = document.querySelector('.result'),
str4 = ''

for(let key in fullProfile)
{
    console.log(key, fullProfile[key])
   
    if(typeof fullProfile[key] === 'object') //костыль, но все же...
    {
        for(let kkk in fullProfile[key])
        {
            // console.log(kkk)
            str4 += `${kkk}: ${fullProfile[key][kkk]}<br>`
        }
    }
    else
    {
        str4 += `${key}: ${fullProfile[key]}<br>`
    }
}
parent4.insertAdjacentHTML('beforeend','<h2 class="task-title">Задача 4</h2>')
parent4.insertAdjacentHTML('beforeend',`<p class="result-list">${str4}<p>`)

console.log('task4', fullProfile)