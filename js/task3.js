const user1 = {
    name: "John",
    years: 30
  };

let parent3 = document.querySelector('.result'),
    {name:name3,years:age, isAdmin = false} = user1 //нельзя повторять названия переменных

console.log('task 3',`name: ${name3}, age: ${age}, isAdmin: ${isAdmin}`)

parent3.insertAdjacentHTML('beforeend','<h2 class="task-title">Задача 3</h2>')
parent3.insertAdjacentHTML('beforeend',`<p class="result-list">name: ${name3}, age: ${age}, isAdmin: ${isAdmin}</h2>`)