const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
  }, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
  }, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
  }];
  
const bookToAdd = {
name: 'Game of thrones',
author: 'George R. R. Martin'
}

let parent5 = document.querySelector('.result'),
    ol5 = document.createElement('ol'),
    newObj = [...books,bookToAdd]

    ol5.className = 'result-list'
console.log('task5',newObj)

newObj.forEach(el => {
    let {name,author} = el
    ol5.insertAdjacentHTML('beforeend',`<li>${name} - ${author}</li>`)
})

parent5.insertAdjacentHTML('beforeend','<h2 class="task-title">Задача 5</h2>')
parent5.insertAdjacentElement('beforeend',ol5)
// parent4.insertAdjacentHTML('beforeend',newObj)